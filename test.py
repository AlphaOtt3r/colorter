from colorter import colorter

print(colorter(msg="Test", fg="light_green", bg="dark_grey", dec="under"))
print(colorter(msg="Test", fg="light_red", bg="red", dec="under"))
print(colorter(msg="Test", fg="blue", bg="white", dec="under"))
print(colorter(msg="Test", fg="light_grey", bg="orange", dec="bold"))